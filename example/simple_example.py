import pandas as pd
from bilan import BilanExtractor

# Charger un fichier tabulaire de bilan
df = pd.read_csv('sample_bilan.csv')

# Calculer les éléments comptable
bilan = BilanExtractor(indicators_list='all',
                       compute_ratios=True,
                       verbose=True)
new_df = bilan.fit_transform(X=df)
print(new_df)
