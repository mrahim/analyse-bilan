#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np


def compute_nb_salarie_moyen(df):
    ''' Nombre de salarié moyen
    '''
    df['_2050'] = df['has_bilan_2050'] * df['YP']
    df['_2033'] = df['has_bilan_2033'] * df['_376']
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_chiffre_affaire_ht(df):
    ''' Chiffre d'affaire hors taxe
    '''
    df['_2050'] = df['has_bilan_2050'] * df['FL']
    df['_2033'] = df['has_bilan_2033'] * \
        (df['_210'] + df['_214'] + df['_218'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_dettes_escompte(df):
    ''' Dettes + effets portés à l'escompte
    '''
    df['_2050'] = df['has_bilan_2050'] * (df['EC'] - df['EB'] + df['YS'])
    df['_2033'] = df['has_bilan_2033'] * (df['_176'] - df['_174'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_capitaux_permanent(df):
    ''' Capitaux Permanents
    '''
    df['_2050'] = df['has_bilan_2050'] * \
        (df['DL'] + df['DO'] + df['DR'] + df['DS'] +
         df['DT'] + df['DU'] + df['DV'] - df['EH'])
    df['_2033'] = df['has_bilan_2033'] * \
        (df['_142'] + df['_154'] + df['_156'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_total_bilan(df):
    ''' Total Bilan
    '''
    df['_2050'] = df['has_bilan_2050'] * \
        (df['EE'] + df['YQ'] + df['YR'] + df['YS'])
    df['_2033'] = df['has_bilan_2033'] * (df['_180'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_ressources(df):
    ''' Ressources durables
    '''
    df['_2050'] = df['has_bilan_2050'] * \
        (df['DL'] + df['DO'] + df['DR'] + df['DS'] + df['DT'] + df['DU'] +
         df['DV'] - df['EH'] + df['ED'] + df['BK'] - df['AA'] - df['CM'])
    df['_2033'] = df['has_bilan_2033'] * \
        (df['_142'] + df['_154'] + df['_156'] + df['_048'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_emploi_stable(df):
    ''' Emplois stables
    '''
    df['_2050'] = df['has_bilan_2050'] * (df['BJ'] + df['CL'] + df['CN'])
    df['_2033'] = df['has_bilan_2033'] * df['_044']
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_disponibilite(df):
    ''' Disponibilités
    '''
    df['_2050'] = df['has_bilan_2050'] * (df['CF'])
    df['_2033'] = df['has_bilan_2033'] * df['_084']
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_valeur_ajoute(df):
    ''' Valeur Ajoutée
    '''
    df['_2050'] = df['has_bilan_2050'] * \
        (df['FL'] + df['FM'] + df['FN'] - df['FS'] -
         df['FT'] - df['FU'] - df['FV'] - df['FW'])
    df['_2033'] = df['has_bilan_2033'] * \
        (df['_210'] + df['_214'] + df['_218'] + df['_222'] + df['_224'] -
         df['_234'] - df['_236'] - df['_238'] - df['_240'] - df['_242'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_fonds_roulement_net(df):
    ''' Fonds de roulement net
    '''
    df['_2050'] = df['has_bilan_2050'] * \
        (df['DL'] + df['DO'] + df['DR'] + df['DS'] + df['DT'] +
         df['DU'] + df['DV'] - df['EH'] + df['ED'] + df['BK'] -
         df['BJ'] - df['AB'] - df['CL'] - df['CM'] - df['CN'])
    df['_2033'] = df['has_bilan_2033'] * \
        (df['_142'] + df['_154'] + df['_156'] + df['_048'] - df['_044'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_immobilisations_brutes(df):
    ''' Immobilisations brutes
    '''
    df['_2050'] = df['has_bilan_2050'] * (df['BJ'])
    df['_2033'] = df['has_bilan_2033'] * (df['_044'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_creances_client_escompte(df):
    ''' Créances clients + effets portés à l’escompte
    '''
    df['_2050'] = df['has_bilan_2050'] * (df['BX'] + df['YS'])
    df['_2033'] = df['has_bilan_2033'] * (df['_068'] + df['_072'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_stock_encours_creance_acompte(df):
    ''' Stocks d'en-cours + créances clients - avances et acomptes reçus
    '''
    df['_2050'] = df['has_bilan_2050'] * \
        (df['BN'] + df['BP'] + df['BX'] - df['DW'])
    df['_2033'] = df['has_bilan_2033'] * \
        (df['_050'] + df['_068'] - df['_164'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_production_globale(df):
    ''' Production globale
    '''
    df['_2050'] = df['has_bilan_2050'] * \
        (df['FL'] + df['FM'] + df['FN'] + df['FO'] + df['FP'] + df['FQ'])
    df['_2033'] = df['has_bilan_2033'] * \
        (df['_210'] + df['_214'] + df['_218'] + df['_222'] +
         df['_224'] + df['_226'] + df['_230'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_actif_circulant_net(df):
    ''' Actif circulant net
    '''
    df['_2050'] = df['has_bilan_2050'] * \
        (df['CJ'] - df['CH'] - df['CK'] - df['CL'])
    df['_2033'] = df['has_bilan_2033'] * \
        (df['_096'] - df['_092'] - df['_098'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_dettes_court_terme(df):
    ''' Dettes à court terme
    '''
    df['_2050'] = df['has_bilan_2050'] * \
        (df['DW'] + df['DX'] + df['DY'] + df['DZ'] + df['EA'] + df['EH'])
    df['_2033'] = df['has_bilan_2033'] * \
        (df['_164'] + df['_166'] + df['_172'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_dettes_fournisseurs(df):
    ''' Dettes fournisseurs
    '''
    df['_2050'] = df['has_bilan_2050'] * (df['DX'])
    df['_2033'] = df['has_bilan_2033'] * (df['_166'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_achats(df):
    ''' Achats
    '''
    df['_2050'] = df['has_bilan_2050'] * \
        (df['FS'] + df['FU'] + df['FW'] + df['YZ'])
    df['_2033'] = df['has_bilan_2033'] * \
        (df['_234'] + df['_238'] + df['_242'] + df['_378'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_tresorerie(df):
    ''' Trésorerie
    '''
    df['_2050'] = df['has_bilan_2050'] * \
        (df['CD'] + df['CF'])
    df['_2033'] = df['has_bilan_2033'] * \
        (df['_080'] + df['_084'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_total_produit(df):
    ''' Total Produit
    '''
    df['_2050'] = df['has_bilan_2050'] * \
        (df['FL'] + df['FM'] + df['FN'] + df['FO'] + df['FP'] + df['FQ'])
    df['_2033'] = df['has_bilan_2033'] * \
        (df['_210'] + df['_214'] + df['_218'] + df['_222'] +
         df['_224'] + df['_226'] + df['_230'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_stocks(df):
    ''' Stocks
    '''
    df['_2050'] = df['has_bilan_2050'] * \
        (df['BL'] + df['BN'] + df['BP'] + df['BR'] + df['BT'])
    df['_2033'] = df['has_bilan_2033'] * \
        (df['_050'] + df['_060'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_frais_financiers(df):
    ''' Frais financiers
    '''
    df['_2050'] = df['has_bilan_2050'] * (df['GR'])
    df['_2033'] = df['has_bilan_2033'] * (df['_294'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_excedent_brut_exploitation(df):
    ''' EBE : excedent brut d'exploitation
    '''
    df['_2050'] = df['has_bilan_2050'] * \
        (df['GG'] + df['GE'] + df['GD'] + df['GC'] +
         df['GB'] + df['GA'] - df['FQ'] - df['FP'])
    df['_2033'] = df['has_bilan_2033'] * \
        (df['_270'] + df['_256'] + df['_254'] + df['_262'] - df['_230'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_chiffre_affaire_subvention_exploitation(df):
    ''' EBE : excedent brut d'exploitation
    '''
    df['_2050'] = df['has_bilan_2050'] * \
        (df['FL'] + df['FO'])
    df['_2033'] = df['has_bilan_2033'] * \
        (df['_210'] + df['_214'] + df['_218'] + df['_226'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_charge_personnel(df):
    ''' Charge de Personnel
    '''
    df['_2050'] = df['has_bilan_2050'] * (df['FY'] + df['FZ'])
    df['_2033'] = df['has_bilan_2033'] * (df['_250'] + df['_252'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_valeur_ajoutee_globale(df):
    ''' Valeur ajoutée globale : VAG
    '''
    df['_2050'] = df['has_bilan_2050'] * \
        (df['FL'] + df['FM'] + df['FN'] - df['FS'] - df['FT'] - df['FU'] -
         df['FV'] - df['FW'] + df['GW'] - df['GG'] + df['HA'] - df['HE'])
    df['_2033'] = df['has_bilan_2033'] * \
        (df['_210'] + df['_214'] + df['_218'] + df['_222'] +
         df['_224'] - df['_234'] - df['_236'] - df['_238'] -
         df['_240'] - df['_242'] + df['_310'] - df['_270'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_capacite_autofinancement(df):
    ''' CAF : Capacité d'autofinancement
    '''
    df['_2050'] = df['has_bilan_2050'] * \
        (df['GW'] - df['FP'] + df['GA'] + df['GB'] +
         df['GC'] + df['GD'] - df['GM'] + df['GQ'] +
         df['HA'] - df['HE'] - df['HJ'] - df['HK'])
    df['_2033'] = df['has_bilan_2033'] * \
        (df['_310'] + df['_254'] + df['_256'] + df['_259'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_endettement_stable(df):
    ''' Endettement stable
    '''
    df['_2050'] = df['has_bilan_2050'] * \
        (df['DS'] + df['DT'] + df['DU'] + df['DV'] - df['EH'])
    df['_2033'] = df['has_bilan_2033'] * \
        (df['_156'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_excedent_brut_global(df):
    ''' EBG : excedent brut global
    '''
    df['_2050'] = df['has_bilan_2050'] * \
        (df['GG'] + df['GE'] + df['GD'] + df['GC'] + df['GB'] + df['GA'] -
         df['FQ'] - df['FP'] + df['GW'] - df['GG'] + df['HA'] - df['HE'])
    df['_2033'] = df['has_bilan_2033'] * \
        (df['_210'] + df['_214'] + df['_218'] + df['_222'] +
         df['_224'] + df['_226'] - df['_234'] - df['_236'] -
         df['_238'] - df['_240'] - df['_242'] - df['_244'] -
         df['_250'] - df['_252'] + df['_310'] - df['_270'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_resultat_net(df):
    ''' Résultat Net
    '''
    df['_2050'] = df['has_bilan_2050'] * (df['HN'])
    df['_2033'] = df['has_bilan_2033'] * (df['_310'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_fonds_propres(df):
    ''' Fonds Propres
    '''
    df['_2050'] = df['has_bilan_2050'] * (df['DL'] + df['DO'])
    df['_2033'] = df['has_bilan_2033'] * (df['_142'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_dotation_amortissement(df):
    ''' Dotation aux Amortissements
    '''
    df['_2050'] = df['has_bilan_2050'] * (df['GA'])
    df['_2033'] = df['has_bilan_2033'] * (df['_254'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_besoins_fonds_roulement(df):
    ''' Besoins en fonds de roulement
    '''
    df['_2050'] = df['has_bilan_2050'] * \
        (df['BL'] + df['BN'] + df['BP'] + df['BR']
         + df['BT'] + df['BX'] - df['DX'])
    df['_2033'] = df['has_bilan_2033'] * \
        (df['_050'] + df['_060'] + df['_068'] - df['_166'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_endettement_financier(df):
    ''' Endettement financier
    '''
    df['_2050'] = df['has_bilan_2050'] * (df['DS'] + df['DT'] + df['DU'])
    df['_2033'] = df['has_bilan_2033'] * (df['_156'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_cout_endettement_financier(df):
    ''' Cout de l'endettement financier
    '''
    df['_2050'] = df['has_bilan_2050'] * (df['GR'] - df['GL'])
    df['_2033'] = df['has_bilan_2033'] * (df['_294'] - df['_280'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_dette_financiere(df):
    ''' Dette financière
    '''
    df['_2050'] = df['has_bilan_2050'] * \
        (df['DS'] + df['DT'] + df['DU'] + df['DV'] +
         2/3 * (df['YQ'] + df['YR']))
    df['_2033'] = df['has_bilan_2033'] * (df['_156'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_reb(df):
    ''' REB
    '''
    df['_2050'] = df['has_bilan_2050'] * \
        (df['GG'] + df['GE'] + df['GD'] + df['GC'] + df['GB'] +
         df['GA'] - df['FQ'] - df['FP'] + df['HP'] + df['HQ'])
    df['_2033'] = df['has_bilan_2033'] * \
        (df['_270'] + df['_254'] + df['_256'] +
         df['_262'] + df['_242'] - df['_230'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_capital_engage(df):
    ''' Capital engagé
    '''
    df['_2050'] = df['has_bilan_2050'] * \
        (df['DL'] + df['DO'] + df['DR'] + df['DS'] + df['DT'] +
         df['DU'] + df['DV'] - df['EH'] + df['ED'] + df['BK'] -
         df['AA'] - df['CM'])
    df['_2033'] = df['has_bilan_2033'] * \
        (df['_142'] + df['_154'] + df['_156'] + df['_048'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_endettement_bdf(df):
    ''' Endettement Banque de France
        Approximation de l'endettement BDF par la formule :
            - 2050: DU + YS (effets escomptés)
            - 2033: 156
    '''
    df['_2050'] = df['has_bilan_2050'] * (df['DU'] + df['YS'])
    df['_2033'] = df['has_bilan_2033'] * (df['_156'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)


def compute_total_endettement(df):
    ''' Approximation de l'endettement global
    '''
    df['_2050'] = df['has_bilan_2050'] * (df['EC'] - df['EB'] + df['YS'])
    df['_2033'] = df['has_bilan_2033'] * (df['_176'] - df['_174'])
    return np.nansum([df['_2050'], df['_2033']], axis=0)
