#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import os
from bilan.elements_comptables import *
from bilan.ratios import *


def _check_columns_format(df):
    ''' TODO : fonction qui vérifie le nom des champs avec un dictionnaire
    de données.
    '''
    pass


def compute_elements_comptables(df, dico, verbose=0):
    for key, value in dico.items():
        func = eval(value)
        df[key] = func(df)
        if verbose > 1:
            print("Successfully computed `{}` indicators.".format(key))
    return df


def compute_ratios(df, dico, verbose=0):
    for key, value in dico.items():
        func = eval(value)
        df[key] = func(df)
        if verbose > 1:
            print("Successfully computed `{}` ratio.".format(key))
    return df


class BilanExtractor():
    """
    Calcul d'indicateur à partir des postes brutes du bilan en tenant compte
    du type de bilan (complet 2050 ou simplifié 2033).

    Parameters:
    -----------
    indicators_list : list,
        Liste d'indicateur à calculer (default 'all').
        Si None ou 'all', tous les indicateurs sont calculés.
        Le nom retourné pour chaque indicateur est contenu dans
        le `dico.json`.

    copy : bool, optionnal
        Faire une copie du DataFrame.

    verbose : int, optionnal
        Paramètre de verbosité, (default 0).

    Exemple d'utilisation :
    -----------------------
    >>> from bilan import BilanExtractor

    >>> df = pd.read_csv(dataframe_path)
    >>> bilan = BilanExtractor(indicators_list=['chiffre_affaire_ht'],
                               verbose=1)
    >>> bilan.fit(X=df)
    >>> new_df = bilan.transform(X=df)
    """

    def __init__(self, indicators_list='all', compute_ratios=True,
                 copy=True, verbose=0):
        CURR_DIR = os.path.dirname(os.path.realpath(__file__))
        json_path = os.path.join(CURR_DIR, '../data/dico.json')
        with open(json_path, 'r') as f:
            self.dico = json.loads(f.read())

        self.indicators_list = indicators_list
        self.compute_ratios = compute_ratios
        self.copy = copy
        self.verbose = verbose
        self.X_ = None
        pass

    def fit(self, X):
        """ Pour la compatibilité scikit-learn:
                - Calcul de 2 Top bilan (2033 et 2050)
                - Vérification du format de colonne

        Parameters:
        -----------
        X : DataFrame, shape = [n_clients, n_colonnes]
            Table des données contenant les postes brutes du bilan en colonne.
            - Le nom des champs doit être dans un format bien précis
              (cf. données `columns_config.csv`).
            - Une ligne doit être renseignée de la manière suivante :
                - Si Bilan 2033 : colonnes `_000` renseignées, `NaN` sinon
                - Si Bilan 2050 : colonnes 'AA' renseignées, `NaN` sion
        """
        if self.copy:
            self.X_ = X.copy()
        else:
            self.X_ = X
        self.X_.columns = [c.upper() for c in self.X_.columns]
        # TODO: check columns format
        # self._check_columns_format(self.X_)
        self.X_['has_bilan_2033'] = (self.X_['_210'].notnull()).astype(int)
        self.X_['has_bilan_2050'] = (self.X_['FL'].notnull()).astype(int)
        return self

    def transform(self, X):
        """Applique le calcul des indicateurs.

        Parameters
        ----------
        X = DataFrame, shape = [n_clients, n_colonnes]
            Tables des données contenants les postes brutes du bilan pour
            un client.

        Returns
        -------
        X_ DataFrame, shape = [n_clients, n_colonnes]
            Tables des données avec les nouveaux indicateurs.
        """
        if self.copy:
            X_ = X.copy()
        else:
            X_ = X
        if self.X_ is not None:
            X_ = self.X_  # check if fitting

        big_dico = self.dico['mapping_elements_comptables']
        if (self.indicators_list is None) or self.indicators_list == 'all':
            dico = big_dico
        else:
            dico = {k: big_dico[k] for k in self.indicators_list}
        X_ = compute_elements_comptables(X_, dico=dico, verbose=self.verbose)

        if self.compute_ratios:
            dico = self.dico['mapping_ratios']
            X_ = compute_ratios(X_, dico=dico, verbose=self.verbose)

        X_.drop(['_2050', '_2033'], axis=1, inplace=True)
        return X_

    def fit_transform(self, X):
        """Applique le calcul des indicateurs.

        Parameters
        ----------
        X = DataFrame, shape = [n_clients, n_colonnes]
            Tables des données contenants les postes brutes du bilan pour
            un client.

        Returns
        -------
        X_ DataFrame, shape = [n_clients, n_colonnes]
            Tables des données avec les nouveaux indicateurs.
        """
        return self.fit(X).transform(X)
