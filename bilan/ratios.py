#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np


def compute_REB_CA(df):
    """ Ratio RR5 : REB  / CA
    """
    formula = (df['bilan_reb'] /
               df['bilan_chiffre_affaire_ht']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_dette_financiere_REB(df):
    """ Ratio RSS11 : Dette financière retraité du crédit bail / REB
    """
    formula = (df['bilan_dette_financiere'] /
               df['bilan_reb']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_cout_endettement_financier_EBE(df):
    """ Ratio RR11 : Coût d'endettement financier net / EBE
    """
    formula = (df['bilan_cout_endettement_financier'] /
               df['bilan_excedent_brut_exploitation']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_endettement_financier_EBE(df):
    """ Ratio RSS13 : Endettement financier brut / EBE
    """
    formula = (df['bilan_endettement_financier'] /
               df['bilan_excedent_brut_exploitation']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_endettement_financier_fonds_propres(df):
    """ Ratio RSS8 : Gearing brut = Endettement financier / Fonds propres
    """
    formula = (df['bilan_endettement_financier'] /
               df['bilan_fonds_propres']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_endettement_financier_CA(df):
    """ Ratio RSS6 :  Endettement financier / Chiffre d’Affaires
    """
    formula = (df['bilan_endettement_financier'] /
               df['bilan_chiffre_affaire_ht']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_fonds_propres_total_bilan(df):
    """ Ratio RSS2 : Fonds propres / Total Bilan
    """
    formula = (df['bilan_fonds_propres'] /
               df['bilan_total_bilan']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_fonds_propres_ressources_durables(df):
    """ Ratio RSS4 (ou R1) : Indépendance financière
            = Fonds Propres / Ressources durables
    """
    formula = (df['bilan_fonds_propres'] /
               df['bilan_ressources']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_endettement_global(df):
    """ Ratio : Endettement globale
            = 360 * (Dettes + effets portés à l'escompte) / CA
    """
    formula = 360 * (df['bilan_dettes_escompte'] /
                     df['bilan_chiffre_affaire_ht']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_capitaux_total_bilan(df):
    """ Ratio RSS1 : Structure = Capitaux permanents / Total bilan
    """
    formula = (df['bilan_capitaux_permanent'] /
               df['bilan_total_bilan']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_ressources_emplois(df):
    ''' Ratio de liquidité :
    Disponibilités / CA
    '''
    formula = (df['bilan_ressources'] /
               df['bilan_emploi_stable']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_dotation_amortissement_immobilisations_brutes(df):
    """ Ratio RSS12 : Dotation aux Amortissements / Immobilisation Brutes
    """
    formula = (df['bilan_dotation_amortissement'] /
               df['bilan_immobilisations_brutes']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_liquidite_generale(df):
    """ Ratio RL2 : Liquidité Générale
            = Actif circulant net / Dettes à court terme
    """
    formula = (df['bilan_actif_circulant_net'] /
               df['bilan_dettes_court_terme']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_disponibilite_CA(df):
    ''' Ratio de liquidité : Disponibilités
    Disponibilités / CA
    '''
    formula = (df['bilan_disponibilite'] /
               df['bilan_chiffre_affaire_ht']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_frais_financiers_CA(df):
    """ Ratio RR1 : Frais Financiers / CA
    """
    formula = (df['bilan_frais_financiers'] /
               df['bilan_chiffre_affaire_ht']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_frais_financiers_EBG(df):
    """ Ratio RR12 : Poids des charges financières = Frais financiers / EBG
    """
    formula = (df['bilan_frais_financiers'] /
               df['bilan_excedent_brut_global']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_CAF_endettement_stable(df):
    """ Ratio RSS9 : CAF / Endettement stable
    """
    formula = (df['bilan_capacite_autofinancement'] /
               df['bilan_endettement_stable']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_EBE_CA_subvention(df):
    """ Ratio RR2 : Taux de marge économique
            = EBE / (CA + subventions d'exploitation)
    """
    formula = (df['bilan_excedent_brut_exploitation'] /
               df['bilan_chiffre_affaire_subvention_exploitation']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_EBE_CA(df):
    """ Ratio RR3 : Tx de marge économique = EBE / CA
    """
    formula = (df['bilan_excedent_brut_exploitation'] /
               df['bilan_chiffre_affaire_ht']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_EBG_VAG(df):
    """ Ratio RR7 : Taux de marge globale = EBG / VAG
    """
    formula = (df['bilan_excedent_brut_global'] /
               df['bilan_valeur_ajoutee_globale']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_VA_CA(df):
    """ Ratio RR6 : Taux de valeur ajoutée = VA / CA
    """
    formula = (df['bilan_valeur_ajoute'] /
               df['bilan_chiffre_affaire_ht']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_EBE_capital_engage(df):
    ''' Ratio de liquidité :
    Disponibilités / CA
    '''
    formula = (df['bilan_excedent_brut_exploitation'] /
               df['bilan_capital_engage']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_EBG_capital_engage(df):
    """ Ratio RRF2 : Rentabilité d'exploitation = EBE / Capital engagé
    """
    formula = (df['bilan_excedent_brut_global'] /
               df['bilan_capital_engage']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_CAF_fonds_propres(df):
    """ Ratio RRF3 : Rentabilité financière des Capitaux propres
            = CAF / Fonds propres
    """
    formula = (df['bilan_capacite_autofinancement'] /
               df['bilan_fonds_propres']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_delais_rotation_besoin_fonds_roulement(df):
    """ Ratio RL5 : Délais de rotation des besoins en fonds de roulement
            = ( BFR  exploitation x 360 ) / CA
    """
    formula = 360 * (df['bilan_besoins_fonds_roulement'] /
                     df['bilan_chiffre_affaire_ht']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_delais_rotation_fonds_roulement(df):
    """ Ratio RL4 : Délais de rotation des fonds de roulement
            = FDR net x 360 / CA
    """
    formula = 360 * (df['bilan_fonds_roulement_net'] /
                     df['bilan_chiffre_affaire_ht']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_delais_rotation_stock(df):
    """ Ratio RL10 : Délai de rotation des stocks = Stocks x 360 / CA
    """
    formula = 360 * (df['bilan_stocks'] /
                     df['bilan_chiffre_affaire_ht']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_delais_rotation_capital(df):
    """ Ratio RRF4 : Délai de rotation du capital
            = Immobilisations brutes x 360 / CA
    """
    formula = 360 * (df['bilan_immobilisations_brutes'] /
                     df['bilan_chiffre_affaire_ht']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_delais_fournisseurs(df):
    """ Ratio RL6 : Délai fournisseurs
            = ( Dettes fournisseurs x 360 ) / achats TTC
    """
    formula = 360 * (df['bilan_dettes_fournisseurs'] /
                     df['bilan_achats']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_delais_clients(df):
    """ Ratio RL9 : Délai clients
            = (Créances clients + effets portés à l’escompte) x 360 / CATTC
    """
    formula = 360 * (df['bilan_creances_client_escompte'] /
                     df['bilan_chiffre_affaire_ht']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_delais_decouverts_clients(df):
    """ Ratio RL8 : Délai découvert clients
            = (Stocks d'en-cours + créances clients -
                avances et acomptes reçus) x 360 / Production globale
    """
    formula = 360 * (df['bilan_stock_encours_creance_acompte'] /
                     df['bilan_production_globale']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_charge_personnel_VAG(df):
    """ Ratio RR8 : Part du personnel dans la valeur ajoutée globale
            = Charges de Personnel / VAG
    """
    formula = (df['bilan_charge_personnel'] /
               df['bilan_valeur_ajoutee_globale']).values
    formula[np.isnan(formula)] = 0
    return formula


def compute_CAF_VAG(df):
    """ Ratio RR9 : Part de l'entreprise dans la valeur ajoutée globale
            = CAF / VAG
    """
    formula = (df['bilan_capacite_autofinancement'] /
               df['bilan_valeur_ajoutee_globale']).values
    formula[np.isnan(formula)] = 0
    return formula
