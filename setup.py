#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
from setuptools import setup


descr = """Projet 89C3 Analyse du bilan"""
DISTNAME = 'bilan'
DESCRIPTION = 'Projet 89C3 Analyse du bilan'
LONG_DESCRIPTION = descr
MAINTAINER = 'Sacha Samama'
MAINTAINER_EMAIL = 'sacha.samama_ext@bpce.fr'
URL = 'bilan'
LICENSE = ''
DOWNLOAD_URL = 'bilan'
VERSION = '0.1'


def configuration(parent_package='', top_path=None):
    if os.path.exists('MANIFEST'):
        os.remove('MANIFEST')

    from numpy.distutils.misc_util import Configuration
    config = Configuration(None, parent_package, top_path)

    config.add_subpackage('bilan')

    return config


if __name__ == "__main__":
    setup(configuration=configuration,
          name=DISTNAME,
          maintainer=MAINTAINER,
          include_package_data=True,
          maintainer_email=MAINTAINER_EMAIL,
          description=DESCRIPTION,
          license=LICENSE,
          url=URL,
          version=VERSION,
          download_url=DOWNLOAD_URL,
          long_description=LONG_DESCRIPTION,
          test_suite="nose.collector",  # for python setup.py test
          zip_safe=False,  # the package can run out of an .egg file
          classifiers=[
              'Intended Audience :: Developers',
              'Programming Language :: C',
              'Programming Language :: Python',
              'Programming Language :: R',
              'Programming Language :: SQL',
              'Topic :: Software Development',
              'Topic :: Scientific/Engineering',
              'Operating System :: Microsoft :: Windows',
              'Operating System :: POSIX',
              'Operating System :: Unix',
              'Operating System :: MacOS'],)
