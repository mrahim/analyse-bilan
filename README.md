# Package Bilan

Cette librairie vise à extraire tout un tas d'indicateurs à partir des postes
du bilan d'une entreprise. Ces indicateurs sont calculés en fonction du type de
bilan :
- Liasse 2033 : bilan simplifié [plus d'infos ici](https://www.impots.gouv.fr/portail/formulaire/2050-liasse/liasse-fiscale-du-regime-reel-normal-en-matiere-de-bic-et-dis)
- Liasse 2050 : bilan complet [plus d'infos ici](https://www.impots.gouv.fr/portail/formulaire/2033-sd/bilan-simplifie)

Elle permet également de calculer des ratios à partir des éléments comptables
pré-calculés (en cours).

#### Architecture du projet

1. Le dossier [data](data/) contient 2 fichiers :
  - [columns.csv](data/columns.csv): fichier contenant pour chaque ligne le nom
  des champ de chaque poste du bilan ainsi que sa signification et sa source
  (liasse 2033 ou liasse 2050).
  - [dico.json](data/dico.json) : dictionnaire de mapping entre les élements
  comptables et les fonctions contenant les formules. La liste des indicateurs
  pré-calculés est détaillée ci-dessous :
      - `achats`:
      - `tresorerie`:
      - `emploi_stable`:
      - `frais_financiers`:
      - `dettes_fournisseurs`:
      - `production_globale`:
      - `disponibilite`:
      - `excedent_brut_exploitation`:
      - `nb_salarie_moyen`:
      - `capacite_autofinancement`:
      - `immobilisations_brutes`:
      - `endettement_stable`:
      - `stocks`:
      - `reb`:
      - `dotation_amortissement`:
      - `creances_client_escompte`:
      - `fonds_propres`:
      - `total_produit`:
      - `valeur_ajoutee_globale`:
      - `chiffre_affaire_subvention_exploitation`:
      - `chiffre_affaire_ht`:
      - `cout_endettement_financier`:
      - `dettes_escompte`:
      - `dettes_court_terme`:
      - `besoins_fonds_roulement`:
      - `fonds_roulement_net`:
      - `capitaux_permanent`:
      - `excedent_brut_global`:
      - `stock_encours_creance_acompte`:
      - `valeur_ajoute`:
      - `endettement_bdf`:
      - `ressources`:
      - `capital_engage`:
      - `charge_personnel`:
      - `total_bilan`:
      - `endettement_financier`:
      - `resultat_net`:
      - `total_endettement`:
      - `actif_circulant_net`:
      - `dette_financiere`:

2. Le dossier [example](example/) contient un extrait de données au format `.csv`
contenant un sous-ensemble de postes brutes du bilan permettant d'extraire
le chiffre d'affaire :
  - `FL` : formule du CA pour la liasse 2050
  - `_210` + `_214` + `_218` : formule du CA pour liasse 2033

#### Installation du package

Télécharger le projet et aller à la racine du répertoire :
```
cd path/to/project/bilan
```

Installation du package :
```
python setup.py install
```

Voir [cet exemple](example/simple_example.py) pour prendre en main la librairie
(à venir).
